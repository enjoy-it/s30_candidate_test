# Installation

```
./s30-sail.sh
cd s30 && ./vendor/bin/sail up
cp .env.example .env
vendor/bin/sail artisan key:generate
vendor/bin/sail artisan migrate
vendor/bin/sail artisan db:seed CreateRoleSeeder
```


# Usage

## POST /users

Sample input:
```
curl --location --request POST 'localhost/users' \
--header 'Accept: application/json' \
--header 'Content-Type: application/json' \
--data-raw '{
  "roles": ["administrator","editor"],
  "email": "john@doe.com",
  "name": "John Doe"
}'
```

Expected output:
```
{
    "message": "User created",
    "data": {
        "id": 6
    }
}
```

## GET /users/role/{id}

Input:
```
curl --location --request GET 'http://localhost/users/role/administrator'
```

Expected output:
```
{
    "users": [
        {
            "id": 4,
            "name": "Test User",
            "email": "another@test.com"
        },
        {
            "id": 6,
            "name": "John Doe",
            "email": "john@doe.com"
        }
    ]
}
```