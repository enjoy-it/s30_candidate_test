<?php

namespace App\Http\Controllers;

use DB;
use Hash;
use App\Models\User;
use App\Models\Role;
use Illuminate\Support\Arr;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\UserFormRequest;

class UserController extends Controller
{
    /**
     * Create user.
     *
     * @param  UserFormRequest $request Form request.
     * @return string                   JSON response.
     */
    public function store(UserFormRequest $request)
    {
        $data = $request->validated();

        try {
            $user = User::create($data);

            $roles = [];
            foreach ($request->input('roles') as $roleName) {
                $roles[] = Role::where('name', '=', $roleName)
                    ->first()->id;
            }

            $user->roles()->attach($roles);

            return response()->json([
                'message' => 'User created',
                'data' => [
                    'id' => $user->id,
                ]
            ]);

        } catch (\Exception $ex) {
            return response()->json([
                'message' => 'Something went wrong: ' . $ex->getMessage(),
            ], 500);
        }
    }

    /**
     * List users assigned to role.
     *
     * @param  string $roleName Role name.
     * @return string           Users.
     */
    public function showRoles($roleName)
    {
        $role = Role::where('name', '=', $roleName)
            ->first();

        if (!$role) {
            return response()->json([
                'message' => 'Role not found',
            ], 404);
        }

        $users = $role->users;

        return response()->json([
            'users' => $users,
        ]);
    }
}
