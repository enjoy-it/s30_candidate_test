<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\User;
use App\Models\Role;

class CreateRoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Role::create(['name' => 'Subscriber']);
        Role::create(['name' => 'Editor']);
        Role::create(['name' => 'Author']);
        Role::create(['name' => 'Administrator']);
    }
}
